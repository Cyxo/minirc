import codecs
import socket
from threading import Thread
from config import *
import oxo
import traceback

context_counter = 0

class Context:
    def __init__(self):
        global context_counter
        self.current_user = None
        self.socket = None
        self.id = context_counter
        self.current_channel = "#hub"
        self.current_game = None
        context_counter += 1

    def send_to_socket(self, msg):
        if self.socket:
            try:
                self.socket.send(codecs.encode(msg + "\n", "utf8"))
            except Exception:
                pass

    def check_current_user(self, login):
        return self.current_user is not None and self.current_user['login'] == login


class QuitCommand(BaseException):
    pass


def handle_client(server, context):
    socket = context.socket
    try:
        while True:
            request = socket.recv(16384).strip()
            request = codecs.decode(request, "utf8")
            result, answer = server.execute(context, request)
            if result:
                prefix = b"+ "
            else:
                prefix = b"- "
            answer = codecs.encode(answer, "utf8")
            socket.send(b"%s%s\n" % (prefix, answer))
    except QuitCommand:
        pass
    except Exception as e:
        print("[C%d] Error while handling the socket (Reason %s)" % (context.id, e))
        print(traceback.format_exc())

    server.destroy_context(context)
    try:
        socket.close()
        print("[C%d] Connection closed" % context.id)
    except Exception:
        pass


class Server:
    def __init__(self, config):
        self.config = config
        self.contexts = []
        if config['userdb']:
            self.users = load_userdb_from_file(config['userdb'])
        else:
            self.users = UserDB()

    def new_context(self):
        result = Context()
        self.contexts.append(result)
        return result

    def destroy_context(self, context):
        self.contexts = list(filter(lambda c : c.id != context.id, self.contexts))

    def execute(self, context, line):
        if line == "":
            return False, "Empty request"
        command_and_args = line.split()
        command = command_and_args[0]
        args = command_and_args[1:]
        if command == "login":
            if len(args) != 2:
                return False, "Syntax: login <user> <password>"
            login, password = args
            login = login.strip()
            password = password.strip()
            context.current_user = self.users.get(login)
            if context.current_user and context.current_user['password'] == password:
                return True, ("Welcome %s" % login)
            else:
                return False, "Invalid user or password"

        elif command == "logout":
            if context.current_user:
                login = context.current_user['login']
                context.current_user = None
                return True, ("Bye %s" % login)
            else:
                return False, "You are not currently logged in"

        elif command == "whoami":
            if context.current_user:
                login = context.current_user['login']
                return True, ("You are %s, silly" % login)
            else:
                return False, "You are not currently logged in"

        elif command == "list":
            if context.current_user:
                logins = self.users.user_list()
                result = []
                for l in logins:
                    relevant_contexts = filter(lambda c: c.check_current_user(l), self.contexts)
                    connected = list(relevant_contexts) != []
                    if connected:
                        result.append("*%s" % l)
                    elif context.current_user['admin']:
                        result.append("%s" % l)
                return True, ", ".join(result)
            else:
                return False, "You are not currently logged in"

        elif command == "print":
            if args == []:
                msg_txt = "nothing"
            else:
                print_cmd, msg_txt = line.split(None, 1)
                msg_txt = "\"%s\"" % msg_txt
            if context.current_user:
                username = context.current_user['login']
            else:
                username = "Anonymous coward"
            channel = context.current_channel
            if channel.startswith("#"):
              msg = "[%s] %s : %s" % (channel, username, msg_txt)
              print(msg)
            else:
              msg = "[PM] %s : %s" % (username, msg_txt)
              print("%s > %s : %s" % (username, channel[1:], msg_txt))
            
            if context.current_channel.startswith("@"):
                for c in filter(lambda c: c.check_current_user(channel[1:]), self.contexts):
                    c.send_to_socket(msg)
                    context.send_to_socket("%s > %s : %s" % (username, channel[1:], msg_txt))
            else:
                for c in filter(lambda c: c.current_channel == channel, self.contexts):
                    c.send_to_socket(msg)
            return True, "Message posted"
        
        elif command == "join":
            if args == []:
                channel = "#hub"
            elif len(args) > 1:
                return False, "Usage: join [channel]"
            else:
                if args[0].startswith("@"):
                    us = list(filter(lambda c: c.check_current_user(args[0][1:]), self.contexts))
                    if len(us) == 0:
                        return False, ("User %s not found" % args[0])
                    else:
                        channel = args[0]
                elif args[0].startswith("#"):
                    channel = args[0]
                else:
                    us = list(filter(lambda c: c.check_current_user(args[0]), self.contexts))
                    if len(us) == 0:
                        channel = "#" + args[0]
                    else:
                        channel = "@" + args[0]
                
            context.current_channel = channel
            return True, ("Joined channel %s" % channel)

        elif command == "oxo":
            if context.current_game is None:
                if not args[0]:
                    return False, "Usage to start a game : oxo [player]"
                else:
                    us = list(filter(lambda c: c.check_current_user(args[0]), self.contexts))
                    if len(us) == 0:
                        return False, ("User %s not found" % args[0])
                    else:
                        game = oxo.Game(context, us[0])
                        context.current_game = game
                        us[0].current_game = game
                        us[0].send_to_socket("[OXO] %s started a game with you\nYou can play with oxo [case]. Example: oxo A2" % (context.current_user["login"] if context.current_user else "Anonymous coward"))
                        return True, ("[OXO] You started a game with %s" % us[0].current_user["login"])
            else:
                if not args[0]:
                    return False, "Usage to play: oxo [case]. Example: A2"
                else:
                    res, mes = context.current_game.play(context, args[0])
                    if res:
                        for player in context.current_game.players:
                            player.send_to_socket("[OXO] " + mes)
                            if res == 2:
                                player.current_game = None
                        return True, "You have played"
                    else:
                        return False, mes
                        
        
        elif command == "help":
            return True, "Valid commands are: help, join, list, login, logout, print, quit, whoami"

        elif command == "quit":
            raise QuitCommand

        else:
            return False, "Invalid command"

    def run(self):
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        address_and_port = self.config['address'], self.config['port']
        server_socket.bind(address_and_port)
        server_socket.listen(5)
        print("[S] Server listening on %s:%d" % address_and_port)

        while True:
            context = self.new_context()
            try:
                s, client_address = server_socket.accept()
                context.socket = s
                print("[C%d] Connection accepted from %s" % (context.id, client_address))
                t = Thread(target=handle_client, args=(self, context))
                t.start()
            except Exception as e:
                print("[C%d] Failed (Reason: %s)" % (context.id, e))
                self.destroy_context(context)


if __name__ == "__main__":
    try:
        config = load_config_from_file("config")
    except Exception:
        config = Config()
    server = Server(config)
    try:
        server.run()
    except KeyboardInterrupt:
        print()
        pass
