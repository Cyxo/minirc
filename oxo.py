import re

class Game:
    def __init__(self, user1, user2):
        self.players = [user1, user2]
        self.grid = []
        for _ in range(3): self.grid.append([" "]*3)
        self.next_player = user2
    
    def render_game(self):
        t = [""]
        t.append('    1   2   3')
        t.append('A | %s |' % (" | ".join(self.grid[0])))
        t.append('  _____________')
        t.append('B | %s |' % (" | ".join(self.grid[1])))
        t.append('  _____________')
        t.append('C | %s |' % (" | ".join(self.grid[2])))
        return "\n".join(t)
    
    def play(self, user, case):
        if user != self.next_player:
            return False, "It's not your turn!"
        elif re.match(r'[A-C][1-3]', case):
            ligne = ord(case[0]) - ord("A")
            col = int(case[1]) - 1
            if self.grid[ligne][col] == " ":
                if self.players.index(user) == 0:
                    self.grid[ligne][col] = "O"
                elif self.players.index(user) == 1:
                    self.grid[ligne][col] = "X"
                
                won = False
                for i in range(len(self.grid)):
                    if self.grid[i][0] == self.grid[i][1] == self.grid[i][2] != " ":
                        won = True
                        g = self.grid[i][0]
                    elif self.grid[0][i] == self.grid[1][i] == self.grid[2][i] != " ":
                        won = True
                        g = self.grid[0][i]
                if self.grid[0][0] == self.grid[1][1] == self.grid[2][2] != " " \
                or self.grid[2][0] == self.grid[1][1] == self.grid[0][2] != " ":
                    won = True
                    g = self.grid[1][1]
                    
                eq = True
                for l in self.grid:
                    for c in l:
                        if c == " ": eq = False
                
                if won:
                    return 2, self.render_game() + "\n" + ("\0%s won the game" % (self.players[0].current_user["login"] if g == "O" else self.players[1].current_user["login"]))
                elif eq:
                    return 2, self.render_game() + "\n" + "\0It's a tie!"
                else:
                    self.next_player = self.players[1 - self.players.index(self.next_player)]
                    return True, self.render_game()
            else:
                return False, "Someone already played at this place"
        else:
            return False, ("%s is not a correct place. Example: oxo A2" % case)